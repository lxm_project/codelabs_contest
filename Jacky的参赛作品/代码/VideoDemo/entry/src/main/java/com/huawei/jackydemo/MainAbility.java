package com.huawei.jackydemo;

import ohos.aafwk.content.Intent;
import ohos.ace.ability.AceAbility;
import ohos.security.SystemPermission;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        requestPremissions();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void requestPremissions() {
        if (verifySelfPermission(SystemPermission.DISTRIBUTED_DATASYNC) != 0) {
            if (canRequestPermission(SystemPermission.DISTRIBUTED_DATASYNC)) {
                requestPermissionsFromUser(new String[]{SystemPermission.DISTRIBUTED_DATASYNC}, 0);
            }
        }
    }
}
