### 演示效果
![](gif/001.gif)

### 视频页面
  
  -JS开发具体页面entry/src/main/js/default/pages/detail/detail.js。

  需在配置文件config.json中添加权限请求ohos.permission.INTERNET。
  
- 在拥有智慧屏作为开发设备。在手机端对应entry中的MainAbility提供了智慧屏debug模式

    - 可以通过将手机和智慧屏两端的entry分别安装在两台手机上，并且在手机端entry中使能debug模式(设置enableDebugMode方法的返回值为true)
    
    - 手机端与大屏端因为系统的差异导致UI效果有些许差别，具体体现在智慧屏会自动为当前焦点绘制方框而手机端没有此特性。
      最终显示效果请以大屏端为参照。
    
    
- 流转能力的规格限制：

    - 调用系统分布式能力实现流转的规格：
    
        - 双端设备系统需要同时具备分布式能力
        
        - 手机可以向其他组网设备包括手机、智慧屏发起流转
        
        - 智慧屏只能作为流转的接收方

# 流转具体实现

- 开发者在阅读代码时应该对以下方面有前置了解：
  
    - JS UI框架实现的FA与继承自AceInternalAbility的PA的通信
    
    - PA与JS UI框架实现的FA通过订阅（subscribe）机制进行通信
    
- 基于系统能力实现页面手机流转到大屏的流程（具体代码参考对应JS UI页面和对应调用的继承自AceInternalAbility的PA）

    - 首先用户在页面点击流转后页面跳转至设备选择界面，此处会显示出当前在线的其他已组网设备
    
    - 获取组网设备代码段如下：
    
      entry\src\main\java\..\DeviceSelectInternalAbility.ACTION_REQUEST_DEVICE_LIST
    
       ```
      
      List<DeviceInfo> devices = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
      
       ```
    
    - 用户选择流转设备，页面根据获取到的对端设备信息判断如果对端设备为智慧屏则跳转至遥控界面否则返回详情界面，并调用分布式拉起方法拉起对端应用
    
    - 拉起对端FA的代码段如下：
    
       - entry/src/main/js/default/pages/detail/detail.js
       
       ```
      async transfer() {
          var forwardCompatible = false;
         ---前向兼容检查 ---  
         .......
         ---获取用户选择的流转设备
          var result = await FeatureAbility.startAbilityForResult(target);
          if (!!forwardCompatible == true) {
              result = JSON.parse(result);
          }
          ......
          ---调用系统分布式拉起能力,拉起对端新闻应用
                  var res1 = await FeatureAbility.startAbility(target);
                  if (!!forwardCompatible == true) {
                      res1 = JSON.parse(res1);
                  }
          ......
          ---判断对端设备类型决定是否进行跳转  
                  if (res1.code == 0 && (targetDeviceType == 'SMART_TV' || deviceDebugMode == true)) {
                 
      ```
    
    
    
    - 连接对端PA的代码段如下：
    
        - DistributeInternalAbility(手机端)#CONNECT_ABILITY
        
        ```
           case CONNECT_ABILITY: {
               ZSONObject dataParsed = ZSONObject.stringToZSON(data.readString());
               selectDeviceId = dataParsed.getString("deviceId");
               RemoteDataSender.connectRemote(selectDeviceId, context);
               assembleReplyResult(replyResult, new Object(), reply);
               break;
           } 
      ```
    
        - commonlib\src\main\java\..\remotedatabus\RemoteDataSender#connectRemote
        
        ```
            public static boolean connectRemote(String deviceId, Context context) {
                RemoteConnection remote = REMOTE_CONNECTION_MAP.get(deviceId);
                if (remote != null) {
                    context.disconnectAbility(remote);
                    REMOTE_CONNECTION_MAP.remove(deviceId);
                }
        
                RemoteConnection newRemote = new RemoteConnection();
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId(deviceId)
                        .withBundleName(context.getBundleName())
                        .withAbilityName(RemoteDataReceiverAbility.class.getName())
                        .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                        .build();
        
                intent.setOperation(operation);
                ---调用系统分布式能力与远端建立持续连接
                boolean isSuccess = context.connectAbility(intent, newRemote);
                HiLog.error(TAG, "connectRemote " + isSuccess);
                if (isSuccess) {
                    REMOTE_CONNECTION_MAP.put(deviceId, newRemote);
                }
                return isSuccess;
            }
        ```
        
   
      
    
    