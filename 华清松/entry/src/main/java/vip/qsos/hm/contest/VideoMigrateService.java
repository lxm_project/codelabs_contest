package vip.qsos.hm.contest;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.rpc.IRemoteObject;
import vip.qsos.hm.contest.manager.idl.VideoMigrationStub;
import vip.qsos.hm.contest.player.api.ImplPlayer;
import vip.qsos.hm.contest.player.constant.Constants;
import vip.qsos.hm.contest.slice.MainAbilitySlice;
import vip.qsos.hm.contest.util.AbilitySliceRouteUtil;

public class VideoMigrateService extends Ability {
    private static final String DESCRIPTOR = "vip.qsos.hm.contest.IVideoIdlInterface";

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    protected IRemoteObject onConnect(Intent intent) {
        return new MyBinder(DESCRIPTOR);
    }

    private class MyBinder extends VideoMigrationStub {

        MyBinder(String descriptor) {
            super(descriptor);
        }

        @Override
        public void flyIn(int index) {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(MainAbility.class.getName())
                    .build();
            intent.setOperation(operation);
            intent.setParam(Constants.INTENT_PLAY_INDEX, index);
            startAbility(intent);
        }

        @Override
        public void playControl(int controlCode, int extras) {
            ImplPlayer player = MainAbilitySlice.mPlayer;
            if (player != null) {
                player.play();
            }
        }

        @Override
        public int flyOut() {
            AbilitySliceRouteUtil.getInstance().terminateSlices();
            return 0;
        }
    }
}
