package vip.qsos.hm.contest.data;

public class VideoTabStyle {
    public static final String BOLD_FONT_NAME = "微软雅黑";
    public static final int INDICATOR_OFFSET = 12;
    public static final int INDICATOR_NORMA_ALPHA = 168;
    public static final int INDICATOR_BONDS = 20;
    public static final int ADVERTISEMENT_SLID_PRO = 3;
    public static final int ADVERTISEMENT_SLID_DELAY = 3;

    private VideoTabStyle() {
    }
}
