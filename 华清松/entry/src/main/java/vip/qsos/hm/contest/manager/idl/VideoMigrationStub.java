package vip.qsos.hm.contest.manager.idl;

import ohos.rpc.*;

public abstract class VideoMigrationStub extends RemoteObject implements IVideoIdlInterface {
    private static final String DESCRIPTOR = "vip.qsos.hm.contest.IVideoIdlInterface";
    private static final int COMMAND_FLY_IN = IRemoteObject.MIN_TRANSACTION_ID;
    private static final int COMMAND_PLAY_CONTROL = IRemoteObject.MIN_TRANSACTION_ID + 1;
    private static final int COMMAND_FLY_OUT = IRemoteObject.MIN_TRANSACTION_ID + 2;

    public VideoMigrationStub(String descriptor) {
        super(descriptor);
    }

    @Override
    public IRemoteObject asObject() {
        return this;
    }

    public static IVideoIdlInterface asInterface(IRemoteObject object) {
        IVideoIdlInterface result = null;
        if (object == null) {
            return result;
        }
        IRemoteBroker broker = object.queryLocalInterface(DESCRIPTOR);
        if (broker != null) {
            if (broker instanceof IVideoIdlInterface) {
                result = (IVideoIdlInterface) broker;
            }
        } else {
            result = new VideoMigrationProxy(object);
        }

        return result;
    }

    @Override
    public boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) throws RemoteException {
        String token = data.readInterfaceToken();
        if (!DESCRIPTOR.equals(token)) {
            return false;
        }
        switch (code) {
            case COMMAND_FLY_IN: {
                int startTimeline = data.readInt();
                flyIn(startTimeline);
                reply.writeNoException();
                return true;
            }
            case COMMAND_PLAY_CONTROL: {
                int controlCode = data.readInt();
                int extras = data.readInt();
                playControl(controlCode, extras);
                reply.writeNoException();
                return true;
            }
            case COMMAND_FLY_OUT: {
                int result;
                result = flyOut();
                reply.writeNoException();
                reply.writeInt(result);
                return true;
            }
            default:
                return super.onRemoteRequest(code, data, reply, option);
        }
    }
}

