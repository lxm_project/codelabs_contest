package vip.qsos.hm.contest.player;

import ohos.agp.graphics.Surface;
import ohos.app.Context;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.media.common.Source;
import ohos.media.player.Player;
import vip.qsos.hm.contest.player.api.ImplLifecycle;
import vip.qsos.hm.contest.player.api.ImplPlayer;
import vip.qsos.hm.contest.player.api.ScreenChangeListener;
import vip.qsos.hm.contest.player.api.StatusChangeListener;
import vip.qsos.hm.contest.player.constant.Constants;
import vip.qsos.hm.contest.player.constant.PlayerStatus;
import vip.qsos.hm.contest.player.factory.SourceFactory;
import vip.qsos.hm.contest.player.manager.HmPlayerLifecycle;
import vip.qsos.hm.contest.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class HmPlayer implements ImplPlayer {
    private static final String TAG = HmPlayer.class.getSimpleName();
    private static final int MICRO_MILLI_RATE = 1000;
    private Player mPlayer;
    private Surface surface;
    private final HmPlayerLifecycle mLifecycle;
    private final Builder mBuilder;
    private PlayerStatus mStatus = PlayerStatus.IDEL;
    private float currentVolume = 1;
    private double videoScale = Constants.NUMBER_NEGATIVE_1;
    private boolean isGestureOpen;

    private final List<StatusChangeListener> statusChangeListeners = new ArrayList<>(0);
    private final List<ScreenChangeListener> screenChangeListeners = new ArrayList<>(0);

    private HmPlayer(Builder builder) {
        mBuilder = builder;
        mLifecycle = new HmPlayerLifecycle(this);
    }

    private void initBasePlayer() {
        mPlayer = new Player(mBuilder.mContext);
        Source source = new SourceFactory(mBuilder.mContext, mBuilder.filePath).getSource();
        mPlayer.setSource(source);
        mPlayer.setPlayerCallback(new HmPlayerCallback());
    }

    private class HmPlayerCallback implements Player.IPlayerCallback {
        @Override
        public void onPrepared() {
            LogUtil.info(TAG, "onPrepared is called ");
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.PREPARED;
                callback.callback(PlayerStatus.PREPARED);
            }
        }

        @Override
        public void onMessage(int info, int i1) {
            LogUtil.info(TAG, "onMessage info is " + info + ",i1 is" + i1);
            if (i1 == 0) {
                switch (info) {
                    case Player.PLAYER_INFO_VIDEO_RENDERING_START:
                        for (StatusChangeListener callback : statusChangeListeners) {
                            mStatus = PlayerStatus.PLAY;
                            callback.callback(PlayerStatus.PLAY);
                        }
                        if (mBuilder.isPause) {
                            pause();
                        }
                        break;
                    case Player.PLAYER_INFO_BUFFERING_START:
                        for (StatusChangeListener callback : statusChangeListeners) {
                            mStatus = PlayerStatus.BUFFERING;
                            callback.callback(PlayerStatus.BUFFERING);
                        }
                        break;
                    case Player.PLAYER_INFO_BUFFERING_END:
                        for (StatusChangeListener callback : statusChangeListeners) {
                            mStatus = PlayerStatus.PLAY;
                            callback.callback(PlayerStatus.PLAY);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        @Override
        public void onError(int type, int extra) {
            LogUtil.info(TAG, "onError is called ,i is " + type + ",i1 is " + extra);
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.ERROR;
                callback.callback(PlayerStatus.ERROR);
            }
            release();
        }

        @Override
        public void onResolutionChanged(int videoX, int videoY) {
            LogUtil.info(TAG, "onResolutionChanged videoX is " + videoX + ",videoY is " + videoY);
            if (!mBuilder.isStretch && videoX != 0 && videoY != 0) {
                videoScale = (double) videoX / videoY;
            }
        }

        @Override
        public void onPlayBackComplete() {
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.COMPLETE;
                callback.callback(PlayerStatus.COMPLETE);
            }
        }

        @Override
        public void onRewindToComplete() {
            resume();
        }

        @Override
        public void onBufferingChange(int value) {
        }

        @Override
        public void onNewTimedMetaData(Player.MediaTimedMetaData mediaTimedMetaData) {
            LogUtil.info(TAG, "onNewTimedMetaData is called");
        }

        @Override
        public void onMediaTimeIncontinuity(Player.MediaTimeInfo mediaTimeInfo) {
            LogUtil.info(TAG, "onMediaTimeIncontinuity is called");
            Player.StreamInfo[] streamInfos = mPlayer.getStreamInfo();
            if (streamInfos != null) {
                for (Player.StreamInfo streamInfo : streamInfos) {
                    int streamType = streamInfo.getStreamType();
                    if (streamType == Player.StreamInfo.MEDIA_STREAM_TYPE_AUDIO && mStatus == PlayerStatus.PREPARED) {
                        for (StatusChangeListener callback : statusChangeListeners) {
                            mStatus = PlayerStatus.PLAY;
                            callback.callback(PlayerStatus.PLAY);
                        }
                        if (mBuilder.isPause) {
                            pause();
                        }
                    }
                }
            }
        }
    }

    private void start() {
        if (mPlayer != null) {
            mBuilder.mContext.getGlobalTaskDispatcher(TaskPriority.DEFAULT).asyncDispatch(() -> {
                if (surface != null) {
                    mPlayer.setVideoSurface(surface);
                } else {
                    LogUtil.error(TAG, "The surface has not been initialized.");
                }
                mPlayer.prepare();
                if (mBuilder.startMillisecond > 0) {
                    int microsecond = mBuilder.startMillisecond * MICRO_MILLI_RATE;
                    mPlayer.rewindTo(microsecond);
                }
                mPlayer.play();
            });
        }
    }

    @Override
    public ImplLifecycle getLifecycle() {
        return mLifecycle;
    }

    @Override
    public void addSurface(Surface videoSurface) {
        this.surface = videoSurface;
    }

    @Override
    public void addPlayerStatusCallback(StatusChangeListener callback) {
        if (callback != null) {
            statusChangeListeners.add(callback);
        }
    }

    @Override
    public void removePlayerStatusCallback(StatusChangeListener callback) {
        statusChangeListeners.remove(callback);
    }

    @Override
    public void addPlayerViewCallback(ScreenChangeListener callback) {
        if (callback != null) {
            screenChangeListeners.add(callback);
        }
    }

    @Override
    public void removePlayerViewCallback(ScreenChangeListener callback) {
        screenChangeListeners.remove(callback);
    }

    @Override
    public Builder getBuilder() {
        return mBuilder;
    }

    @Override
    public PlayerStatus getPlayerStatus() {
        return mStatus;
    }

    @Override
    public void resizeScreen(int width, int height) {
        for (ScreenChangeListener screenChangeCallback : screenChangeListeners) {
            screenChangeCallback.screenCallback(width, height);
        }
    }

    @Override
    public void openGesture(boolean isOpen) {
        isGestureOpen = isOpen;
    }

    @Override
    public boolean isGestureOpen() {
        return isPlaying() && isGestureOpen;
    }

    @Override
    public void play() {
        if (mPlayer != null) {
            mPlayer.reset();
        }
        for (StatusChangeListener callback : statusChangeListeners) {
            mStatus = PlayerStatus.PREPARING;
            callback.callback(PlayerStatus.PREPARING);
        }
        initBasePlayer();
        start();
    }

    @Override
    public void replay() {
        if (isPlaying()) {
            rewindTo(0);
        } else {
            reload(mBuilder.filePath, 0);
        }
    }

    @Override
    public void reload(String filepath, int startMillisecond) {
        mBuilder.filePath = filepath;
        mBuilder.startMillisecond = startMillisecond;
        play();
    }

    @Override
    public void stop() {
        if (mPlayer == null) {
            return;
        }
        mPlayer.stop();
        for (StatusChangeListener callback : statusChangeListeners) {
            mStatus = PlayerStatus.STOP;
            callback.callback(PlayerStatus.STOP);
        }
    }

    @Override
    public void release() {
        if (mPlayer == null) {
            return;
        }
        if (mStatus != PlayerStatus.IDEL) {
            videoScale = Constants.NUMBER_NEGATIVE_1;
            mPlayer.release();
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.IDEL;
                callback.callback(PlayerStatus.IDEL);
            }
        }
    }

    @Override
    public void resume() {
        if (mPlayer == null) {
            return;
        }
        if (mStatus != PlayerStatus.IDEL) {
            if (!isPlaying()) {
                mPlayer.play();
            }
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.PLAY;
                callback.callback(PlayerStatus.PLAY);
            }
        }
    }

    @Override
    public void pause() {
        if (mPlayer == null) {
            return;
        }
        if (isPlaying()) {
            mPlayer.pause();
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.PAUSE;
                callback.callback(PlayerStatus.PAUSE);
            }
        }
    }

    @Override
    public int getCurrentPosition() {
        if (mPlayer == null) {
            return 0;
        }
        return mPlayer.getCurrentTime();
    }

    @Override
    public int getDuration() {
        if (mPlayer == null) {
            return 0;
        }
        return mPlayer.getDuration();
    }

    @Override
    public float getVolume() {
        return currentVolume;
    }

    @Override
    public void setVolume(float volume) {
        if (mPlayer != null) {
            if (mPlayer.setVolume(volume)) {
                currentVolume = volume;
            }
        }
    }

    @Override
    public void setPlaySpeed(float speed) {
        if (mPlayer == null) {
            return;
        }
        if (mStatus != PlayerStatus.IDEL) {
            mPlayer.setPlaybackSpeed(speed);
        }
    }

    @Override
    public double getVideoScale() {
        return videoScale;
    }

    @Override
    public boolean isPlaying() {
        if (mPlayer != null) {
            return mPlayer.isNowPlaying();
        }
        return false;
    }

    @Override
    public void rewindTo(int startMicrosecond) {
        if (mPlayer == null) {
            return;
        }
        if (mStatus != PlayerStatus.IDEL) {
            for (StatusChangeListener callback : statusChangeListeners) {
                mStatus = PlayerStatus.BUFFERING;
                callback.callback(PlayerStatus.BUFFERING);
            }
            mPlayer.rewindTo(startMicrosecond * MICRO_MILLI_RATE);
        }
    }

    public static class Builder {
        private final Context mContext;
        private String filePath;
        private int startMillisecond;
        private boolean isStretch;
        private boolean isPause;

        public Builder(Context context) {
            mContext = context;
            filePath = "";
            startMillisecond = 0;
        }

        public Builder setFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public String getFilePath() {
            return filePath;
        }

        public Builder setStartMillisecond(int startMillisecond) {
            this.startMillisecond = startMillisecond;
            return this;
        }

        public int getStartMillisecond() {
            return startMillisecond;
        }

        public Builder setStretch(boolean isS) {
            this.isStretch = isS;
            return this;
        }

        public Builder setPause(boolean isP) {
            this.isPause = isP;
            return this;
        }

        public ImplPlayer create() {
            return new HmPlayer(this);
        }
    }
}
