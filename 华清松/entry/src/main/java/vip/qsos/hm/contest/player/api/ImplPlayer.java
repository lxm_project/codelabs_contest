package vip.qsos.hm.contest.player.api;

import ohos.agp.graphics.Surface;
import vip.qsos.hm.contest.player.HmPlayer;
import vip.qsos.hm.contest.player.constant.PlayerStatus;

public interface ImplPlayer {
    void addSurface(Surface surface);

    void addPlayerStatusCallback(StatusChangeListener callback);

    void removePlayerStatusCallback(StatusChangeListener callback);

    void addPlayerViewCallback(ScreenChangeListener callback);

    void removePlayerViewCallback(ScreenChangeListener callback);

    void play();

    void replay();

    void reload(String filepath, int startMillisecond);

    void resume();

    void pause();

    int getCurrentPosition();

    int getDuration();

    float getVolume();

    void setVolume(float volume);

    void setPlaySpeed(float speed);

    double getVideoScale();

    void rewindTo(int startMicrosecond);

    boolean isPlaying();

    void stop();

    void release();

    ImplLifecycle getLifecycle();

    HmPlayer.Builder getBuilder();

    PlayerStatus getPlayerStatus();

    void resizeScreen(int width, int height);

    void openGesture(boolean isOpen);

    boolean isGestureOpen();
}
