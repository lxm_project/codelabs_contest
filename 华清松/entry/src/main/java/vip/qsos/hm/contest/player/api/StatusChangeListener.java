package vip.qsos.hm.contest.player.api;

import vip.qsos.hm.contest.player.constant.PlayerStatus;

public interface StatusChangeListener {
    void callback(PlayerStatus status);
}