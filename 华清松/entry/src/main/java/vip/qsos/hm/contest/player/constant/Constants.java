package vip.qsos.hm.contest.player.constant;

public class Constants {
    public static final int NUMBER_NEGATIVE_1 = -1;
    public static final int NUMBER_2 = 2;
    public static final float NUMBER_FLOAT_2 = 2f;
    public static final int NUMBER_10 = 10;
    public static final int NUMBER_25 = 25;
    public static final int NUMBER_36 = 36;
    public static final int NUMBER_40 = 40;
    public static final int NUMBER_100 = 100;
    public static final int NUMBER_168 = 168;
    public static final int NUMBER_300 = 300;
    public static final int NUMBER_150 = 150;
    public static final int NUMBER_1000 = 1000;
    public static final int PLAYER_PROGRESS_RUNNING = 0;
    public static final int PLAYER_CONTROLLER_HIDE = 1;
    public static final int PLAYER_CONTROLLER_SHOW = 2;
    public static final int REWIND_STEP = 5000;
    public static final String INTENT_PLAY_INDEX = "INTENT_PLAY_INDEX";

    private Constants() {
    }
}
