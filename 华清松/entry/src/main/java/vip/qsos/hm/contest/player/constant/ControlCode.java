package vip.qsos.hm.contest.player.constant;

public enum ControlCode {
    RESUME(1001),
    PAUSE(1002),
    STOP(1003),
    SEEK(1004),
    FORWARD(1005),
    REWARD(1006),
    VOLUME_ADD(1007),
    VOLUME_REDUCED(1008),
    VOLUME_SET(1009);

    private final int code;

    ControlCode(int value) {
        this.code = value;
    }

    public int getCode() {
        return code;
    }
}
