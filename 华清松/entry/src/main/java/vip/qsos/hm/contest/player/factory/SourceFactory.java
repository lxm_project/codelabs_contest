package vip.qsos.hm.contest.player.factory;

import ohos.app.Context;
import ohos.global.resource.RawFileDescriptor;
import ohos.media.common.Source;
import vip.qsos.hm.contest.util.LogUtil;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

public class SourceFactory {
    private static final String TAG = "SourceFactory";
    private static final String NET_HTTP_MATCH = "http";
    private static final String NET_RTMP_MATCH = "rtmp";
    private static final String NET_RTSP_MATCH = "rtsp";
    private static final String STORAGE_MATCH = "/storage/";

    private Source mPlayerSource;

    public SourceFactory(Context context, String path) {
        try {
            initSourceType(context, path);
        } catch (IOException e) {
            LogUtil.error(TAG, "Audio resource is unavailable: ");
        }
    }

    private void initSourceType(Context context, String path) throws IOException {
        if (context == null || path == null) {
            return;
        }
        if (path.substring(0, NET_HTTP_MATCH.length()).equalsIgnoreCase(NET_HTTP_MATCH)
                || path.substring(0, NET_RTMP_MATCH.length()).equalsIgnoreCase(NET_RTMP_MATCH)
                || path.substring(0, NET_RTSP_MATCH.length()).equalsIgnoreCase(NET_RTSP_MATCH)) {
            mPlayerSource = new Source(path);
        } else if (path.startsWith(STORAGE_MATCH)) {
            File file = new File(path);
            if (file.exists()) {
                FileInputStream fileInputStream = new FileInputStream(file);
                FileDescriptor fileDescriptor = fileInputStream.getFD();
                mPlayerSource = new Source(fileDescriptor);
            }
        } else {
            RawFileDescriptor fd = context.getResourceManager().getRawFileEntry(path).openRawFileDescriptor();
            mPlayerSource = new Source(fd.getFileDescriptor(), fd.getStartPosition(), fd.getFileSize());
        }
    }

    public Source getSource() {
        return mPlayerSource;
    }
}
