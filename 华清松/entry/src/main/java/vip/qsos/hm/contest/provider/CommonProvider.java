package vip.qsos.hm.contest.provider;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public abstract class CommonProvider<T> extends BaseItemProvider {
    protected List<T> data;
    protected Context context;
    protected int layoutId;

    public CommonProvider(Context context, final int layoutId) {
        this(context, layoutId, new ArrayList<>(0));
    }

    public CommonProvider(Context context, int layoutId, List<T> data) {
        this.data = data;
        this.context = context;
        this.layoutId = layoutId;
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public T getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer parent) {
        ViewProvider holder = ViewProvider.get(context, component, parent, layoutId, position);

        convert(holder, getItem(position), position);
        return holder.getComponentView();
    }

    protected abstract void convert(ViewProvider holder, T item, int position);
}
