package vip.qsos.hm.contest.provider;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.HashMap;

public class ViewProvider {
    protected int componentPosition;
    protected int layoutId;
    private final Component component;
    private final HashMap<Integer, Component> views;

    public ViewProvider(Context context, Component itemView, ComponentContainer parent, int position) {
        component = itemView;
        this.componentPosition = position;
        views = new HashMap<Integer, Component>(0);
        component.setTag(this);
    }

    public static ViewProvider get(Context context, Component convertView, ComponentContainer parent, int layoutId, int position) {
        if (convertView == null) {
            Component itemView = LayoutScatter.getInstance(context).parse(layoutId, null, false);
            ViewProvider viewProvider = new ViewProvider(context, itemView, parent, position);
            viewProvider.layoutId = layoutId;
            return viewProvider;
        } else {
            ViewProvider viewProvider = null;
            Object object = convertView.getTag();
            if (object instanceof ViewProvider) {
                viewProvider = (ViewProvider) object;
                viewProvider.componentPosition = position;
            }
            return viewProvider;
        }
    }

    public <T extends Component> T getView(int viewId) {
        Component view = views.get(viewId);
        if (view == null) {
            view = component.findComponentById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }

    public Component getComponentView() {
        return component;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void updatePosition(int position) {
        this.componentPosition = position;
    }

    public int getItemPosition() {
        return componentPosition;
    }

    public void setText(int viewId, String text) {
        Text tv = getView(viewId);
        tv.setText(text);
    }

    public void setImageResource(int viewId, int resId) {
        Image image = getView(viewId);
        image.setPixelMap(resId);
        image.setScaleMode(Image.ScaleMode.STRETCH);
    }

    public ViewProvider setOnClickListener(int viewId, Component.ClickedListener listener) {
        Component newComponent = getView(viewId);
        newComponent.setClickedListener(listener);
        return this;
    }

    public ViewProvider setOnTouchListener(int viewId, Component.TouchEventListener listener) {
        Component newComponent = getView(viewId);
        newComponent.setTouchEventListener(listener);
        return this;
    }
}
