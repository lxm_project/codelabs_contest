package vip.qsos.hm.contest.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.bundle.AbilityInfo;
import ohos.bundle.ElementName;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;
import vip.qsos.hm.contest.MainAbility;
import vip.qsos.hm.contest.ResourceTable;
import vip.qsos.hm.contest.VideoMigrateService;
import vip.qsos.hm.contest.component.Toast;
import vip.qsos.hm.contest.data.VideoRes;
import vip.qsos.hm.contest.manager.idl.IVideoIdlInterface;
import vip.qsos.hm.contest.manager.idl.VideoMigrationStub;
import vip.qsos.hm.contest.player.HmPlayer;
import vip.qsos.hm.contest.player.api.ImplPlayer;
import vip.qsos.hm.contest.player.constant.Constants;
import vip.qsos.hm.contest.player.view.PlayerLoading;
import vip.qsos.hm.contest.player.view.PlayerView;
import vip.qsos.hm.contest.provider.CommonProvider;
import vip.qsos.hm.contest.provider.VideoProvider;
import vip.qsos.hm.contest.provider.ViewProvider;
import vip.qsos.hm.contest.util.AbilitySliceRouteUtil;
import vip.qsos.hm.contest.util.DialogUtils;
import vip.qsos.hm.contest.util.LogUtil;
import vip.qsos.hm.contest.util.MediaUtil;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = MainAbility.class.getSimpleName();
    private static final int TOAST_DURATION = 3000;

    public static ImplPlayer mPlayer;
    private final List<DeviceInfo> devices = new ArrayList<>(0);
    private List<Component> componentList = new ArrayList<>();

    private IVideoIdlInterface iVideoIdlInterface;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_video);
        componentList = getVideoComponents();
        initView();
    }

    private void initView() {
        Component advViewPager = findComponentById(ResourceTable.Id_video_vp);
        if (advViewPager instanceof PageSlider) {
            VideoProvider<Component> provider = new VideoProvider<>(componentList);
            PageSlider pageSlider = (PageSlider) advViewPager;
            pageSlider.setProvider(provider);
            pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
                @Override
                public void onPageSliding(int i, float v, int i1) {
                }

                @Override
                public void onPageSlideStateChanged(int i) {
                }

                @Override
                public void onPageChosen(int i) {
                    if (mPlayer != null) {
                        mPlayer.stop();
                    }
                    Component player = componentList.get(i).findComponentById(ResourceTable.Id_player_view);
                    if (player instanceof PlayerView) {
                        mPlayer = ((PlayerView) player).getPlayer();
                        mPlayer.play();
                    }
                }
            });
        } else {
            LogUtil.debug(TAG, "initAdvertisement failed, advertisement_viewpager is not PageSlider");
        }

        getUITaskDispatcher().delayDispatch(() -> {
            if (mPlayer != null) {
                mPlayer.play();
            }
        }, 1000);

    }

    private List<Component> getVideoComponents() {
        List<VideoRes> videoRes = MediaUtil.getVideoData();
        List<Component> componentList = new ArrayList<>(videoRes.size());
        for (int i = 0; i < videoRes.size(); i++) {
            VideoRes video = videoRes.get(i);
            Component videoItemView = LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_item_video, null, false);

            ImplPlayer player = new HmPlayer.Builder(this)
                    .setFilePath(video.getSourceUrl())
                    .setStartMillisecond(0)
                    .create();
            player.getLifecycle().onStart();
            Component playerView = videoItemView.findComponentById(ResourceTable.Id_player_view);
            if (playerView instanceof PlayerView) {
                ((PlayerView) playerView).bind(player);
            }
            if (i == 0) {
                mPlayer = player;
            }
            Component loading = videoItemView.findComponentById(ResourceTable.Id_loading_view);
            if (loading instanceof PlayerLoading) {
                ((PlayerLoading) loading).bind(player);
            }
            Component videoDesc = videoItemView.findComponentById(ResourceTable.Id_item_video_desc);
            if (videoDesc instanceof Text) {
                ((Text) videoDesc).setText(video.getDescription());
            }
            Component comment = videoItemView.findComponentById(ResourceTable.Id_item_video_comment);
            comment.setClickedListener(component -> {
                new DialogUtils().showCommentDialog(this, video.getComments());
            });
            Component connect = videoItemView.findComponentById(ResourceTable.Id_item_video_connect);
            connect.setClickedListener(component -> {
                showDevice();
            });
            componentList.add(videoItemView);
        }

        return componentList;
    }

    private void showDevice() {
        initDevices();
        showDeviceList();
    }

    private void initDevices() {
        if (devices.size() > 0) {
            devices.clear();
        }
        List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        devices.addAll(deviceInfoList);
    }

    private void startAbilityFa(String devicesId) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId(devicesId)
                .withBundleName(getBundleName())
                .withAbilityName(VideoMigrateService.class.getName())
                .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                .build();
        intent.setOperation(operation);
        boolean connectFlag = connectAbility(intent, new IAbilityConnection() {
            @Override
            public void onAbilityConnectDone(ElementName elementName, IRemoteObject remoteObject, int extra) {
                iVideoIdlInterface = VideoMigrationStub.asInterface(remoteObject);
                try {
                    iVideoIdlInterface.flyIn(0);
                } catch (RemoteException e) {
                    LogUtil.error(TAG, "connect successful,but have remote exception");
                }
            }

            @Override
            public void onAbilityDisconnectDone(ElementName elementName, int extra) {
                disconnectAbility(this);
            }
        });
        if (connectFlag) {
            Toast.toast(this, "已流转", TOAST_DURATION);
            mPlayer.release();
        } else {
            Toast.toast(this, "流转失败", TOAST_DURATION);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
        getGlobalTaskDispatcher(TaskPriority.DEFAULT).delayDispatch(() -> {
            if (mPlayer != null && !mPlayer.isPlaying()) {
                mPlayer.play();
            }
        }, Constants.NUMBER_1000);
    }

    @Override
    protected void onInactive() {
        LogUtil.info(TAG, "onInactive is called");
        super.onInactive();
    }

    @Override
    public void onForeground(Intent intent) {
        mPlayer.getLifecycle().onForeground();
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        LogUtil.info(TAG, "onBackground is called");
        mPlayer.getLifecycle().onBackground();
        super.onBackground();
    }

    @Override
    protected void onStop() {
        LogUtil.info(TAG, "onStop is called");
        AbilitySliceRouteUtil.getInstance().removeRoute(this);
        mPlayer.getLifecycle().onStop();
        super.onStop();
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        mPlayer.openGesture(displayOrientation == AbilityInfo.DisplayOrientation.LANDSCAPE);
    }

    private CommonDialog dialog;

    private void showDeviceList() {
        // 设备列表dialog
        dialog = new CommonDialog(this);
        dialog.setAutoClosable(true);
        dialog.setTitleText("转播");
        dialog.setSize(700, 500);

        ListContainer devicesListContainer = new ListContainer(getContext());
        CommonProvider<DeviceInfo> commonProvider = new CommonProvider<DeviceInfo>(this, ResourceTable.Layout_device_list_item, devices) {
            @Override
            protected void convert(ViewProvider holder, DeviceInfo item, int position) {
                holder.setText(ResourceTable.Id_device_text, item.getDeviceName());
                Text text = holder.getView(ResourceTable.Id_device_text);
                text.setText(item.getDeviceName());
            }
        };
        devicesListContainer.setItemProvider(commonProvider);
        devicesListContainer.setItemClickedListener((listContainer, component, position, id) -> {
            dialog.destroy();
            startAbilityFa(devices.get(position).getDeviceId());
        });
        commonProvider.notifyDataChanged();
        dialog.setContentCustomComponent(devicesListContainer);
        dialog.show();
    }
}
