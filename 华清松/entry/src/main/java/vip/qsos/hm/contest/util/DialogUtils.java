package vip.qsos.hm.contest.util;

import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import vip.qsos.hm.contest.ResourceTable;
import vip.qsos.hm.contest.provider.CommonProvider;
import vip.qsos.hm.contest.provider.ViewProvider;

import java.util.ArrayList;

public class DialogUtils {

    public void showCommentDialog(Context context, ArrayList<String> comments) {
        CommonDialog dialog = new CommonDialog(context);
        Component commentView = LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_dialog_comment, null, false);
        TextField commentInput = (TextField) commentView.findComponentById(ResourceTable.Id_dialog_comment_input);
        Component commentClose = commentView.findComponentById(ResourceTable.Id_dialog_comment_close);
        Text commentSend = (Text) commentView.findComponentById(ResourceTable.Id_dialog_comment_send);
        ListContainer commentList = (ListContainer) commentView.findComponentById(ResourceTable.Id_dialog_comment_list);
        CommonProvider<String> commonProvider = new CommonProvider<String>(context, ResourceTable.Layout_device_list_item, comments) {
            @Override
            protected void convert(ViewProvider holder, String item, int position) {
                holder.setText(ResourceTable.Id_device_text, item);
                Text text = holder.getView(ResourceTable.Id_device_text);
                text.setText(item);
            }
        };
        commentList.setItemProvider(commonProvider);
        commonProvider.notifyDataChanged();
        dialog.setContentCustomComponent(commentView);
        commentClose.setClickedListener(component -> {
            dialog.destroy();
        });
        commentSend.setClickedListener(component -> {
            String comment = commentInput.getText();
            comments.add(comment);
            commonProvider.notifyDataChanged();
        });
        dialog.setAlignment(LayoutAlignment.BOTTOM);
        dialog.setOffset(0, 0);
        dialog.show();
    }
}
