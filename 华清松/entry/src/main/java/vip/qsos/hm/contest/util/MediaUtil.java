package vip.qsos.hm.contest.util;

import vip.qsos.hm.contest.ResourceTable;
import vip.qsos.hm.contest.data.VideoRes;

import java.util.ArrayList;
import java.util.List;

public class MediaUtil {
    private static final List<VideoRes> VIDEO_VIDEO_RES = new ArrayList<>(0);

    private MediaUtil() {
    }

    static {
        VIDEO_VIDEO_RES.add(new VideoRes(ResourceTable.Media_video_ad0, "entry/resources/base/media/test0.mp4", "HDC2021"));
        VIDEO_VIDEO_RES.add(new VideoRes(ResourceTable.Media_video_ad2, "entry/resources/base/media/test2.mp4", "我期待"));
        VIDEO_VIDEO_RES.add(new VideoRes(ResourceTable.Media_video_ad3, "entry/resources/base/media/test0.mp4", "HarmonyOS"));
    }

    public static List<VideoRes> getVideoData() {
        return VIDEO_VIDEO_RES;
    }

}
