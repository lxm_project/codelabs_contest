/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lzf.mytestnews.ability;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.lxj.xpopup.util.ElementUtil;
import com.lzf.mytestnews.bean.NewsInfo;
import com.lzf.mytestnews.provider.VideoListAdapter;
import com.lzf.mytestnews.utils.*;
import com.lzf.mytestnews.view.TikTokRenderViewFactory;
import com.yc.kernel.utils.VideoLogUtils;
import com.yc.video.bridge.ControlWrapper;
import com.yc.video.config.ConstantKeys;
import com.yc.video.player.SimpleStateListener;
import com.yc.video.player.VideoPlayer;
import com.yc.video.tool.PlayerUtils;
import com.yc.video.ui.view.BasisVideoController;
import com.yc.videocache.cache.PreloadManager;
import com.yc.videocache.cache.ProxyVideoCacheManager;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import com.lzf.mytestnews.ResourceTable;

import java.util.ArrayList;
import java.util.List;

/**
 * 模仿抖音短视频，使用VerticalViewPager实现，实现了预加载功能
 *
 * @since 2021-05-11
 */
public class VideoplayerAbility extends BaseAbility implements IAbilityContinuation {
    private static final String KEY_INDEX = "index";
    /**
     * 当前播放位置
     */
    private int mCurPos = -1;
    private List<NewsInfo> mVideoList = new ArrayList<>();
    private VideoListAdapter mTiktok2Adapter;
    private PreloadManager mPreloadManager;
    private VideoPlayer mVideoPlayer;
    private Image mBack;
    private PageSlider mViewPager;
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private String playUrl;
    private Context context;
    private int index = 0;
    private boolean isPush = false;

    /**
     * 启动
     *
     * @param context 上下文
     * @param index   指标
     */
    public static void start(Context context, int index) {
        Intent intent = new Intent();
        intent.setOperation(new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(VideoplayerAbility.class)
                .build());
        intent.setParam(KEY_INDEX, index);
        context.startAbility(intent, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mVideoPlayer != null) {
            mVideoPlayer.release();
        }
        mPreloadManager.removeAllPreloadTask();
        // 清除缓存，实际使用可以不需要清除，这里为了方便测试
        ProxyVideoCacheManager.clearAllCache(this);
    }

    @Override
    protected void onActive() {
        super.onActive();
        if (mVideoPlayer != null) {
            mVideoPlayer.resume();
        }
    }

    @Override
    public void onBackPressed() {
        ControlWrapper.defaultValue();
        if (mVideoPlayer == null || !mVideoPlayer.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        if (mVideoPlayer != null) {
            mVideoPlayer.pause();
        }
    }

    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_ability_tik_tok2;
    }

    @Override
    protected void initComponent(Intent intent) {
        super.initComponent(intent);
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_vvp);
        context = this.getContext();
        initView();
    }

    /**
     * 初始化视图
     */
    protected void initView() {
        // 修改状态栏的颜色
        StatusBarUtils.setStatusBarColor(this, ElementUtil.getColor(this, ResourceTable.Color_colorBar));
        initViewPager();
        initVideoView();
        mPreloadManager = PreloadManager.getInstance(this);
        if (isPush == false) {
            Intent extras = getIntent();
            index = extras.getIntParam(KEY_INDEX, 0);
        }
        LogUtils.error("index:", "" + index);
        mViewPager.setCurrentPage(index);

        runMainThread(new Runnable() {
            @Override
            public void run() {
                startPlay(index);
            }
        });
    }

    /**
     * 切换任务到主线程执行
     *
     * @param runnable 执行的runnable
     */
    public void runMainThread(Runnable runnable) {
        eventHandler.postTask(runnable);
    }

    private void initVideoView() {
        mVideoPlayer = new VideoPlayer(this);
        ComponentContainer decorView = (ComponentContainer) findComponentById(ResourceTable.Id_tok_layout);
        mVideoPlayer.setDecorView(decorView);
        ComponentContainer.LayoutConfig layoutConfig = mVideoPlayer.getLayoutConfig(); // 播放器居中
        layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
        mVideoPlayer.setLooping(true);

        // 以下只能二选一，看你的需求
        mVideoPlayer.setRenderViewFactory(TikTokRenderViewFactory.create());
        mController = new BasisVideoController(this);
        mVideoPlayer.setController(mController);
        // 监听播放结束
        mVideoPlayer.addOnStateChangeListener(new SimpleStateListener() {
            @Override
            public void onPlayStateChanged(int playState) {
                if (playState == ConstantKeys.CurrentState.STATE_BUFFERING_PLAYING) {
                    runMainThread(new Runnable() {
                        @Override
                        public void run() {
                            playVideo();
                        }
                    });
                }
            }
        });

        mBack = (Image) findComponentById(ResourceTable.Id_back);
        mBack.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onBackPressed();
            }
        });

    }

    /**
     * 重复播放
     */
    private void playVideo() {
        mVideoPlayer.release();
        mController.removeAllControlComponent();
        mController.addDefaultControlComponent("");

        ComponentContainer directionalLayout = (ComponentContainer) mViewPager.getComponentAt(0);
        Component itemView = directionalLayout.getComponentAt(mCurPos);
        VideoListAdapter.ViewHolder viewHolder = (VideoListAdapter.ViewHolder) itemView.getTag();
        ControlWrapper controlWrapper = mController.getControlWrapper();
        if (controlWrapper != null) {
            controlWrapper.setToggleState(0);
            controlWrapper.setToggleText(viewHolder.mTitle);
            controlWrapper.setToggleControlView(viewHolder.mTikTokView);
        }
        // 重新设置数据
        mVideoPlayer.setUrl(playUrl);

        // 开始播放
        mVideoPlayer.start();
    }

    private void initViewPager() {
        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_vvp);

        Gson gson = new Gson();
        List<NewsInfo> totalNewsDataList =
                gson.fromJson(
                        CommonUtils.getStringFromJsonPath(this, "entry/resources/rawfile/news_datas.json"),
                        new TypeToken<List<NewsInfo>>() {
                        }.getType());

        mVideoList.addAll(totalNewsDataList);
        mViewPager.setPageCacheSize(ConstantVideo.getVideoList().size());
        mTiktok2Adapter = new VideoListAdapter(context, mVideoList);
        mViewPager.setProvider(mTiktok2Adapter);
        mViewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            private int mCurItem;

            /**
             * VerticalViewPager是否反向滑动
             */
            private boolean mIsReverseScroll;

            @Override
            public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
                if (position == mCurItem) {
                    return;
                }
                mIsReverseScroll = position < mCurItem;
            }

            @Override
            public void onPageSlideStateChanged(int state) {
                if (state == PageSlider.SLIDING_STATE_DRAGGING) {
                    mCurItem = mViewPager.getCurrentPage();
                }
                if (state == PageSlider.SLIDING_STATE_IDLE) {
                    mPreloadManager.resumePreload(mCurPos, mIsReverseScroll);
                } else {
                    mPreloadManager.pausePreload(mCurPos, mIsReverseScroll);
                }
            }

            @Override
            public void onPageChosen(int position) {
                if (position == mCurPos) {
                    return;
                }
                runMainThread(new Runnable() {
                    @Override
                    public void run() {
                        startPlay(position);
                    }
                });
            }
        });
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        int ordinal = displayOrientation.ordinal();
        if (ordinal == 1) {
            mViewPager.setSlidingPossible(false);
        } else {
            mViewPager.setSlidingPossible(true);
        }
    }

    private void startPlay(int position) {
        if (mCurPos == position) {
            return;
        }
        ComponentContainer directionalLayout = (ComponentContainer) mViewPager.getComponentAt(0);
        Component itemView = directionalLayout.getComponentAt(position);
        if (itemView == null) {
            return;
        }
        VideoListAdapter.ViewHolder viewHolder = (VideoListAdapter.ViewHolder) itemView.getTag();
        if (viewHolder.mPosition == position) {
            mVideoPlayer.release();
            PlayerUtils.removeViewFormParent(mVideoPlayer);

            NewsInfo tiktokBean = mVideoList.get(position);
            playUrl = mPreloadManager.getPlayUrl(tiktokBean.getVideoUrl());
            VideoLogUtils.i("startPlay: " + "position: " + position + "  url: " + playUrl);
            mVideoPlayer.setUrl(playUrl);
            mVideoPlayer.setScreenScaleType(ConstantKeys.PlayerScreenScaleType.SCREEN_SCALE_16_9);

            ControlWrapper controlWrapper = mController.getControlWrapper();
            if (controlWrapper != null) {
                controlWrapper.setToggleState(0);
                controlWrapper.setToggleText(viewHolder.mTitle);
                controlWrapper.setToggleControlView(viewHolder.mTikTokView);
            }
            mController.addControlComponent(viewHolder.mTikTokView, true);
            viewHolder.mPlayerContainer.addComponent(mVideoPlayer, 0);
            viewHolder.mDlPush.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
//                    DialogUtils.toast(context, "分布式流转", 2000);
                    // 查询设备
                    List<DeviceInfo> deviceList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
                    if (deviceList.size() == 0) {
                        DialogUtils.toast(context, "暂无流转设备", 2000);
                        return;
                    }

                    String[] dialogArr = new String[deviceList.size()];
                    for (int i = 0; i < deviceList.size(); i++) {
                        dialogArr[i] = deviceList.get(i).getDeviceName();
                    }


                    new XPopup.Builder(getContext())
                            .isDarkTheme(false)
                            .enableDrag(true)
                            .asBottomList("请选择设备", dialogArr,
                                    new OnSelectListener() {
                                        @Override
                                        public void onSelect(int position, String text) {
                                            continueAbility();
                                        }
                                    })
                            .show();
                }
            });
            mVideoPlayer.start();
            mCurPos = position;
        }
    }

    //Page请求迁移后
    @Override
    public boolean onStartContinuation() {
        // 请求迁移后，进入此回调
        return true;
    }

    //onSaveData()
    @Override
    public boolean onSaveData(IntentParams intentParams) {
        // 保存传递的数据
        intentParams.setParam("currentPosition", index);
        return true;
    }


    //源侧设备上Page完成保存数据后，系统在目标侧设备上回调此方法，开发者在此回调中接受用于恢复Page状态的数据。
    @Override
    public boolean onRestoreData(IntentParams intentParams) {
        // 此方法在生命周期onStart之前调用
        // 获取传递过来的数据
        index = (int) intentParams.getParam("currentPosition");
        isPush = true;
        return true;
    }

    // 迁移完成
    @Override
    public void onCompleteContinuation(int i) {
        terminateAbility();
    }
}
