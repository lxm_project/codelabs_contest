//xxx.js
export default {
    data: {
        event:'',
        seekingtime:'',
        timeupdatetime:'',
        seekedtime:'',
        isStart: true,
        isfullscreenchange: false,
        duration: '',
        continueAbilityData: {
            showView:false,
            index:0,
            todolist: [{
                           src:'/common/ccc.mp4',
                           commentList:[
                               {user:'user01',comment:'视频不错'}
                           ]
                       },
                       {
                           src:'/common/aaa.mp4'
                       }],
    },



    },
     shareDate:{
         todolist2: [
                    {
                        src:'/common/aaa.mp4'
                    }],

        },
    onInit(){
//        this.$element('pingLunQuer').close()

    },
    preparedCallback:function(e){ this.event = '视频连接成功'; this.duration = e.duration;},
    startCallback:function(){ this.event = '视频开始播放';},
    pauseCallback:function(){ this.event = '视频暂停播放'; },
    finishCallback:function(){ this.event = '视频播放结束';},
    errorCallback:function(){ this.event = '视频播放错误';},
    seekingCallback:function(e){ this.seekingtime = e.currenttime; },
    timeupdateCallback:function(e){ this.timeupdatetime = e.currenttime;},
    change_start_pause: function() {
        if(this.isStart) {
            this.$element('videoId').pause();
            this.isStart = false;
        } else {
            this.$element('videoId').start();
            this.isStart = true;
        }
    },
    change_fullscreenchange: function() {//全屏
        if(!this.isfullscreenchange) {
            this.$element('videoId').requestFullscreen({ screenOrientation : 'default' });
            this.isfullscreenchange = true;
        } else {
            this.$element('videoId').exitFullscreen();
            this.isfullscreenchange = false;
        }
    },
//    liuzhuan(){
//        console.info("点击流转")
//    },
     tryContinueAbility: async function(e) {
         console.info("流转数据序号:"+e)
         this.continueAbilityData.index = e;
    // 应用进行迁移
    let result = await FeatureAbility.continueAbility();

    console.info("迁移结果result:" + JSON.stringify(result));
  },
  onStartContinuation() {
    // 判断当前的状态是不是适合迁移
    console.info("onStartContinuation");
    return true;
  },
  onCompleteContinuation(code) {
    // 迁移操作完成，code返回结果
    console.info("CompleteContinuation: code = " + code);
  },
  onSaveData(saveData) {
      console.info("分享数据为："+JSON.stringify(saveData))

    // 数据保存到savedData中进行迁移。
    var data = this.continueAbilityData;
    Object.assign(saveData, data)
      console.info("保存数据为："+JSON.stringify(saveData))
  },
    onRestoreData(restoreData) {
        console.info("收到数据："+restoreData)
        console.info("收到数据："+JSON.stringify(restoreData))
        console.info("收到数据索引："+restoreData.index)
        let idx = restoreData.index;
        console.info("收到数据值："+JSON.stringify(restoreData.todolist[idx]))
        restoreData.todolist = restoreData.todolist[idx]
        console.info("更新后数据："+JSON.stringify(restoreData))
        // 收到迁移数据，恢复。
        this.continueAbilityData = restoreData;
    },
//    changePager(e){
//        console.info("滑动索引："+e.index)
//        this.continueAbilityData.index = e;
//
//    },
    pinglun(){
        let state = this.continueAbilityData.showView
        console.info("输入评论"+state)
        this.continueAbilityData.showView = !state

    }
}
