# #HarmonyOS挑战赛第二期# JS仿小红书视频播放
## 前言
  无聊的时候大家都喜欢刷刷视频，小红书这个软件视频功能做的也是很受欢迎的，本工程是仿小红书实现的视频播放软件。
## 功能
  视频播放、暂停、下滑切换视频、点赞、收藏、评论、流转、弹幕
## 实现过程
### 实现效果
 ![](./img/1.gif)
### 创建工程
 使用模板Video Player Ability,创建VideoPlayer应用
 ![](./img/1.png)
### 代码实现
#### 视频播放暂停
  - 使用组件video[官方文档](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-media-video-0000000000611764)来载入视频，隐藏视频的控制器
```html
<video
      id="{{ item.id }}"
      class="swiper-item"
      src="{{ item.url }}"
      autoplay='true'
      loop='true'
      controls="false"
      onclick="controlsClick({{ item.id }})"
      ></video>
```
  - 视频的播放暂停通过点击事件来实现
```js
if (this.isShow) {
    this.$element(id).start();
    prompt.showToast({
        message: '视频已播放',
        duration: 3000,
    });
} else {
    this.$element(id).pause();
    prompt.showToast({
        message: '视频已暂停',
        duration: 3000
    });
}
this.isShow = !this.isShow
  ```
#### 切换视频
 - 使用官方组件swiper来实现[官方文档](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-container-swiper-0000000000611533) 
 ```html
<swiper 
    class="video-content" 
    id="video-content" 
    index="{{ swiperIndex }}" 
    indicator="false" 
    loop="true" 
    vertical="true" 
    onchange="swiperChange">
</swiper>
 ```
 - 滑动后会调用change事件，改变保存的当前处于第几个index
 ```js
swiperChange(e) {
    this.swiperIndex = e.index;
},
 ```
#### 点赞、收藏
 - 增加点赞收藏的结构
 ```html
<image src="{{ item.sc ? '/common/images/scy.png' : '/common/images/sc.png' }}" @click="imgClick('sc', {{ index }}})"></image>
<image src="{{ item.dz ? '/common/images/dzy.png' : '/common/images/dz.png' }}" @click="imgClick('dz', {{ index }}})"></image>
 ```
 - 点击后将值改为相反即可
 ```js
imgClick(type, index) {
    this.videoList[index][type] = !this.videoList[index][type]
},
 ``` 
 #### 评论
 - 增加结构输入框，提交按钮，评论列表
 ```html
<div class="pl-content" if="{{ swiperIndex === index }}">
    <text if="{{ showPl && swiperIndex === index }}" class="close" @click="plClick">
        X
    </text>
    <list if="{{ showPl && swiperIndex === index }}" class="pl-list">
        <list-item for="{{ i in item.pl }}" class="todo-item">
            <text class="todo-title">● {{ i }}</text>
        </list-item>
    </list>
    <div class="input-content">
        <input
                id="input"
                class="input"
                type="text"
                value="{{ inputValue }}"
                enterkeytype="send"
                placeholder="请输入您的评论"
                onchange="changeInput"
                onenterkeyclick="buttonClick">
        </input>
        <input class="button"
                type="button"
                value="发送"
                onclick="buttonClick({{ index }})"
                ></input>
    </div>
</div>
 ```
 - 当前点击评论按钮，让评论列表展示出来
 - input输入框中的值改变的时候，保存至data数据中
 - 点击发送，评论列表新增数据，同时弹幕上飘过新内容
 ```js
//    点击发送 后触发
buttonClick(index) {
    this.videoList[index].pl.push(this.inputValue)
    this.randerMarquee(this.inputValue)
},
//    生成随机跑马灯数据
randerMarquee(value) {
    const index = Math.floor(Math.random() * 3)
    const i = Math.floor(Math.random() * this.videoList[this.swiperIndex].pl.length)
    if (this.marqueeList[index].show === false) {
        this.marqueeList[index].show = true
        this.marqueeList[index].value = value ? value : this.videoList[this.swiperIndex].pl[i]
        if (value) this.inputValue = ''
    } else {
        this.randerMarquee()
    }
},
 ``` 
 #### 流转
 - 发起流转
 ```js
async transfer() {
    const target = {};
    target.bundleName = 'com.example.videoplayer';
    target.abilityName = 'DeviceSelectAbility';
    target.deviceType = 1;
    this.deviceId = '';
    var result = await FeatureAbility.startAbilityForResult(target);
    if (result.code == 0) {
        var dataRemote = JSON.parse(result.data);
        var data;
        data = dataRemote.result;
        const targetDeviceId = data.deviceId;
        if (!!targetDeviceId && (targetDeviceId != this.deviceId)) {
            this.deviceId = targetDeviceId;
            const target = {};
            target.bundleName = 'com.example.videoplayer';
            target.abilityName = 'MainAbility';
            target.deviceId = this.deviceId;
            target.data = {
                swiperIndex: this.swiperIndex,
                isTransmit: true
            };
            var res1 = await FeatureAbility.startAbility(target);
            if (res1.code === 0) {
                prompt.showToast({
                    message: '流转成功, 即将关闭本应用',
                    duration: 2000,
                });
                setTimeout(() => {
                    FeatureAbility.finishWithResult(100);
                }, 2000)
            }
        }
    }
},
 ```
 - 接受流转数据
 ```js
async onNewRequest() {
    if (this.isTransmit === true) {
        this.$element('video-content').swipeTo({index: this.swiperIndex});
    }
}
 ```
 ### 权限申请，在config.json下申请
```json
"reqPermissions": [
    {
    "name": "ohos.permission.INTERNET"
    },
    {
    "name": "ohos.permission.DISTRIBUTED_DATASYNC"
    },
    {
    "name": "ohos.permission.DISTRIBUTED_DEVICE_STATE_CHANGE"
    },
    {
    "name": "ohos.permission.GET_DISTRIBUTED_DEVICE_INFO"
    },
    {
    "name": "ohos.permission.GET_BUNDLE_INFO"
    },
    {
    "name": "ohos.permission.KEEP_BACKGROUND_RUNNING"
    }
]
```
### 结尾
 此代码已上传至gitee,有需要的小伙伴可下载查阅。https://gitee.com/shi-fan-a/codelabs_contest
## 作者：石凡