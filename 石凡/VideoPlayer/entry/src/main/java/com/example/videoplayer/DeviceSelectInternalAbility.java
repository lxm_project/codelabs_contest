package com.example.videoplayer;

import ohos.ace.ability.AceInternalAbility;
import ohos.app.AbilityContext;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.MessageOption;
import ohos.rpc.MessageParcel;
import ohos.rpc.RemoteException;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * deviceSelectInternalAbility declare business logic to react according
 * to messageCode sent from FA(written by javascript)
 */
class DeviceSelectInternalAbility extends AceInternalAbility {
    private static final int ACTION_REQUEST_DEVICE_LIST = 1001;
    private static final HiLogLabel TAG = new HiLogLabel(0, 0, "DeviceSelectInternalAbility");
    private static DeviceSelectInternalAbility INSTANCE;
    private final AbilityContext context;

    /**
     * constructor of DeviceSelectInternalAbility
     *
     * @param context ability context
     */
    private DeviceSelectInternalAbility(AbilityContext context) {
        super("com.example.videoplayer.phone", "DeviceSelectInternalAbility");
        this.context = context;
    }

    /**
     * set InternalAbilityHandler for instance of DeviceSelectInternalAbility
     *
     * @param context ability context
     */
    static void register(AbilityContext context) {
        INSTANCE = new DeviceSelectInternalAbility(context);
        INSTANCE.setInternalAbilityHandler((code, data, reply, option)
                -> INSTANCE.onRemoteRequest(code, data, reply, option));
    }

    /**
     * react to remote request due to different ACTION_CODE, you should make sure ACTION_CODE is same at FA and PA.
     *
     * @param code   ACTION_CODE
     * @param data   currently excessive
     * @param reply  reply for remote request
     * @param option currently excessive
     * @return whether request is correctly reply
     * @throws RemoteException when action for remote request went wrong
     */
    private boolean onRemoteRequest(int code, MessageParcel data, MessageParcel reply, MessageOption option) throws
            RemoteException {
        if (code == ACTION_REQUEST_DEVICE_LIST) {
            context.requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"}, 0);
            Map<String, Object> replyResult = new HashMap<>();
            replyResult.put("code", 0);
            ZSONArray zsonArray = new ZSONArray();
            List<DeviceInfo> devices = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
            HiLog.info(TAG, "Find online device count:" + devices.size());
            for (DeviceInfo deviceInfo : devices) {
                ZSONObject obj = new ZSONObject();
                obj.put("name", deviceInfo.getDeviceName());
                obj.put("type", deviceInfo.getDeviceType());
                obj.put("id", deviceInfo.getDeviceId());
                obj.put("debugMode", MainAbility.enableDebugMode());
                zsonArray.add(obj);
            }
            replyResult.put("devices", zsonArray);
            reply.writeString(ZSONObject.toZSONString(replyResult));
        } else {
            throw new RemoteException();
        }

        return true;
    }
}
