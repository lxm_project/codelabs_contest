package com.mydemo.topnews.constant;

/**
 * MediaConstant
 */
public class MediaConstant {
    /**
     * ohos.resource
     */
    public static final String OHOS_RESOURCE_SCHEME_RAW = "ohos.resource";
    /**
     * ohos.resource://
     */
    public static final String OHOS_RESOURCE_SCHEME = OHOS_RESOURCE_SCHEME_RAW + "://";
    /**
     * resources/base/media/
     */
    public static final String RAW_PATH = "resources/base/media/";
    /**
     * Invalid value. This value is not updated when the time or progress is transferred.
     */
    public static final int INVALID_VALUE = -1;
}
