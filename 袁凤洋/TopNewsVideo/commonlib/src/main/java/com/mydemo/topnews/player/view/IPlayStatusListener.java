package com.mydemo.topnews.player.view;

import com.mydemo.topnews.player.core.PlayerStatus;

/**
 * Playback status listener.
 */
public interface IPlayStatusListener {
    /**
     * Update of playback status.
     *
     * @param status the next playback status.
     */
    void onPlayStatusChange(PlayerStatus status);
}
