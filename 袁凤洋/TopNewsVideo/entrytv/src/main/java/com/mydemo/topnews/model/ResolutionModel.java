package com.mydemo.topnews.model;

import com.mydemo.topnews.constant.Constants;
import com.mydemo.topnews.utils.PathUtils;

/**
 * Resolution Model Class
 */
public class ResolutionModel {
    private String name;

    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        if (!PathUtils.isUri(url)) {
            return Constants.LOCAL_RESOURCE_PATH + url;
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
