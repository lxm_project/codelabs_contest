

package com.huawei.codelab;

import ohos.rpc.IRemoteBroker;
import ohos.rpc.RemoteException;

public interface ImplVideoMigration extends IRemoteBroker {

    void flyIn(
        /* [in] */ int startTimemiles) throws RemoteException;

    void playControl(
        /* [in] */ int controlCode,
        /* [in] */ int extras) throws RemoteException;

    int flyOut() throws RemoteException;
};

