/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.code.distributedvideo.provider;

import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Slider;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.bundle.ElementName;
import ohos.code.distributedvideo.ResourceTable;
import ohos.code.distributedvideo.VideoMigrateService;
import ohos.code.distributedvideo.component.Toast;
import ohos.code.distributedvideo.data.VideoListMo;
import ohos.code.distributedvideo.manager.idl.ImplVideoMigration;
import ohos.code.distributedvideo.manager.idl.VideoMigrationStub;
import ohos.code.distributedvideo.player.HmPlayer;
import ohos.code.distributedvideo.player.api.ImplPlayer;
import ohos.code.distributedvideo.player.constant.Constants;
import ohos.code.distributedvideo.player.view.PlayerLoading;
import ohos.code.distributedvideo.player.view.PlayerView;
import ohos.code.distributedvideo.util.LogUtil;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ohos.data.search.schema.PhotoItem.TAG;

/**
 * 界面滑动效果
 *
 * @author: ColorPicker
 * @since 2021-04-19
 */
public class PagerProvider extends PageSliderProvider {
    // 数据源，每个页面对应list中的一项
    private List<DataItem> list;
    private Context context;
    private Text textTitle;
    private static final int TOAST_DURATION = 3000;
    private static ImplPlayer player;
    private ImplVideoMigration implVideoMigration;
    private DependentLayout parentLayout;
    private ListContainer deviceListContainer;
    private List<DeviceInfo> devices = new ArrayList<>(0);
    private int startMillisecond = 0;
    private Image pinglun_click;
    private Image switch_click;
    private CommonProvider<VideoListMo> commonProvider;
    private ListContainer playListContainer = null;
    private TextField send_message;
    private Text zanwu_pinglun;
    private Button send_message_button;
    private PageSlider pageSlider;
    private static List<VideoListMo> playListMos = new ArrayList<>();

    /**
     * 构造函数
     *
     * @param listInfo
     * @param con
     */
    public PagerProvider(List<DataItem> listInfo, Context con, PageSlider pageSlider) {
        this.context = con;
        this.pageSlider = pageSlider;
        this.list = listInfo;
        player = new HmPlayer.Builder(context).setFilePath("entry/resources/base/media/TwinklingRefreshLayout.mp4").setStartMillisecond(startMillisecond).create();
    }

    /**
     * 获取数量
     *
     * @return
     */
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * 创建子布局
     *
     * @param componentContainer
     * @param i
     * @return
     */
    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        System.out.println("========createPageInContainer========" + i);
        final DataItem data = list.get(i);
        StackLayout layout = (StackLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_video_play_layout,
                        null, false);
        componentContainer.addComponent(layout);

        pinglun_click = (Image) layout.findComponentById(ResourceTable.Id_pinglun_click);
        switch_click = (Image) layout.findComponentById(ResourceTable.Id_switch_click);
        pinglun_click.setClickedListener(new Component.ClickedListener() {//评论
            @Override
            public void onClick(Component component) {
                emoteImage();
                initList();
            }
        });
        switch_click.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                initDevices();
                menu();
            }
        });

//        if (pageSlider.getCurrentPage() == i) {
        player.getLifecycle().onStart();
        initComponent(layout);
        context.getGlobalTaskDispatcher(TaskPriority.DEFAULT).delayDispatch(() -> player.play(), Constants.NUMBER_1000);
//        }
        return layout;
    }

    /**
     * 这种移除布局
     *
     * @param componentContainer
     * @param i
     * @param o
     */
    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    /**
     * 设置页面转换
     *
     * @param component
     * @param o
     * @return
     */
    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return true;
    }

    /**
     * DataItem
     *
     * @author: ColorPicker
     * @since 2021-04-19
     */
    public static class DataItem {
        private String mText;

        /**
         * 构造函数
         *
         * @param txt
         */
        public DataItem(String txt) {
            mText = txt;
        }
    }

    private void initList() {
        if (playListMos.size() > 0) {
            commonProvider = new CommonProvider<VideoListMo>(
                    playListMos,
                    context,
                    ResourceTable.Layout_pinglun_gv_item) {
                @Override
                protected void convert(ViewProvider holder, VideoListMo item, int position) {
                    holder.setText(ResourceTable.Id_name_tv, item.getName());
                    holder.setText(ResourceTable.Id_content_tv, item.getDescription());
                    holder.setImageResource(ResourceTable.Id_image_iv, item.getSourceId());
                }
            };
            playListContainer.setItemProvider(commonProvider);
        }
    }

    public void emoteImage() {
        Optional<Display>
                display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();

        PopupDialog menuDialog = new PopupDialog(context, null);
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_dialog_view, null, false);
        menuDialog.setCustomComponent(layout);
        menuDialog.setSize(displayAttributes.width, AttrHelper.fp2px(500, context));
        menuDialog.setAutoClosable(true);
        menuDialog.setAlignment(LayoutAlignment.BOTTOM);
        menuDialog.show();
        send_message = (TextField) layout.findComponentById(ResourceTable.Id_send_message);
        zanwu_pinglun = (Text) layout.findComponentById(ResourceTable.Id_zanwu_pinglun);
        send_message_button = (Button) layout.findComponentById(ResourceTable.Id_send_message_button);
        playListContainer = (ListContainer) layout.findComponentById(ResourceTable.Id_pinglun_list_play_view);
        send_message.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean b) {
                if (b) {
                    // 获取到焦点
                    System.out.println("获取焦点");
                    send_message.setAdjustInputPanel(true);
                } else {
                    // 失去焦点
                    System.out.println("失去焦点");
                    send_message.setAdjustInputPanel(false);
                }
            }
        });
        send_message_button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!send_message.getText().equals("")) {
                    zanwu_pinglun.setVisibility(Component.HIDE);
                    playListMos.add(new VideoListMo(ResourceTable.Media_science6,
                            "陈建朋", send_message.getText().toString()));
                    if (commonProvider != null) {
                        commonProvider.notifyDataChanged();
                    } else {
                        initList();
                    }
                    send_message.setText("");
                    send_message.setHint("请输入信息...");
                } else {
                    Toast.toast(context, "请输入信息", 2);
                }
            }
        });

        if (playListMos.size() > 0) {
            zanwu_pinglun.setVisibility(Component.HIDE);
        } else {
            zanwu_pinglun.setVisibility(Component.VISIBLE);
        }
    }

    /**
     * getImplPlayer
     *
     * @return ImplPlayer
     */
    public static ImplPlayer getImplPlayer() {
        return player;
    }

    private void initComponent(StackLayout layout) {
        menu();
        if (layout.findComponentById(ResourceTable.Id_player_view) instanceof PlayerView) {
            PlayerView playerView = (PlayerView) layout.findComponentById(ResourceTable.Id_player_view);
            playerView.bind(player);
        }
        if (layout.findComponentById(ResourceTable.Id_loading_view) instanceof PlayerLoading) {
            PlayerLoading playerLoading = (PlayerLoading) layout.findComponentById(ResourceTable.Id_loading_view);
            playerLoading.bind(player);
        }
    }

    private void initDevices() {
        if (devices.size() > 0) {
            devices.clear();
        }
        List<DeviceInfo> deviceInfos = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
        devices.addAll(deviceInfos);
    }

    private void showDeviceList() {
        CommonProvider commonProvider = new CommonProvider<DeviceInfo>(
                devices,
                context,
                ResourceTable.Layout_device_list_item) {
            @Override
            protected void convert(ViewProvider holder, DeviceInfo item, int position) {
                holder.setText(ResourceTable.Id_device_text, item.getDeviceName());
                Button clickButton = holder.getView(ResourceTable.Id_device_text);
                clickButton.setText(item.getDeviceName());
                clickButton.setClickedListener(component -> {
                    startAbilityFa(item.getDeviceId());
                });
            }
        };
        deviceListContainer.setItemProvider(commonProvider);
        commonProvider.notifyDataChanged();
    }

    public void menu() {
        Optional<Display>
                display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();

        PopupDialog menuDialog = new PopupDialog(context, null);
        DirectionalLayout menuComponent = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_trans_slide, null, false);
        menuComponent.setWidth(AttrHelper.vp2px(displayAttributes.width, context));
        menuDialog.setCustomComponent(menuComponent);
        menuDialog.setAutoClosable(true);
        deviceListContainer = (ListContainer) menuComponent.findComponentById(ResourceTable.Id_device_list_container);
        menuDialog.setAlignment(LayoutAlignment.VERTICAL_CENTER);
        menuDialog.show();

        showDeviceList();
    }

    private void startAbilityFa(String devicesId) {
        Intent intent = new Intent();
        Operation operation =
                new Intent.OperationBuilder()
                        .withDeviceId(devicesId)
                        .withBundleName(context.getBundleName())
                        .withAbilityName(VideoMigrateService.class.getName())
                        .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                        .build();
        intent.setOperation(operation);
        boolean connectFlag = context.connectAbility(intent, new IAbilityConnection() {
            @Override
            public void onAbilityConnectDone(ElementName elementName, IRemoteObject remoteObject, int extra) {
                implVideoMigration = VideoMigrationStub.asInterface(remoteObject);
                try {
                    implVideoMigration.flyIn(startMillisecond);
                } catch (RemoteException e) {
                    LogUtil.error(TAG, "connect successful,but have remote exception");
                }
            }

            @Override
            public void onAbilityDisconnectDone(ElementName elementName, int extra) {
                context.disconnectAbility(this);
            }
        });
        if (connectFlag) {
            Toast.toast(context, "transmit successful！", TOAST_DURATION);
            startMillisecond = player.getCurrentPosition();
            player.release();
        } else {
            Toast.toast(context, "transmit failed!Please try again later.", TOAST_DURATION);
        }
    }
}
