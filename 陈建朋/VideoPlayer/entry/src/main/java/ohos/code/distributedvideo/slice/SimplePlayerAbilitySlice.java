/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.code.distributedvideo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.bundle.AbilityInfo;
import ohos.bundle.ElementName;
import ohos.code.distributedvideo.ResourceTable;
import ohos.code.distributedvideo.VideoMigrateService;
import ohos.code.distributedvideo.component.Toast;
import ohos.code.distributedvideo.data.VideoListMo;
import ohos.code.distributedvideo.manager.idl.ImplVideoMigration;
import ohos.code.distributedvideo.manager.idl.VideoMigrationStub;
import ohos.code.distributedvideo.player.HmPlayer;
import ohos.code.distributedvideo.player.api.ImplPlayer;
import ohos.code.distributedvideo.player.constant.Constants;
import ohos.code.distributedvideo.player.view.PlayerLoading;
import ohos.code.distributedvideo.player.view.PlayerView;
import ohos.code.distributedvideo.provider.CommonProvider;
import ohos.code.distributedvideo.provider.PagerProvider;
import ohos.code.distributedvideo.provider.ViewProvider;
import ohos.code.distributedvideo.util.AbilitySliceRouteUtil;
import ohos.code.distributedvideo.util.LogUtil;
import ohos.code.distributedvideo.util.ScreenUtils;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ohos.data.search.schema.PhotoItem.TAG;

/**
 * PlayerAbilitySlice
 *
 * @since 2020-12-04
 */
public class SimplePlayerAbilitySlice extends AbilitySlice {
    private static final String TAG = SimplePlayerAbilitySlice.class.getSimpleName();
    private static List<VideoListMo> playListMos = new ArrayList<>();
    private PageSlider pageSlider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_simple_video_play_layout);
        AbilitySliceRouteUtil.getInstance().addRoute(this);
        initPageSlider();
    }

    private void initPageSlider() {
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider);
        pageSlider.setProvider(new PagerProvider(getData(), getContext(),pageSlider));
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {

            }
        });
    }

    private ArrayList<PagerProvider.DataItem> getData() {
        ArrayList<PagerProvider.DataItem> dataItems = new ArrayList<>();
        dataItems.add(new PagerProvider.DataItem("entry/resources/base/media/TwinklingRefreshLayout.mp4"));
        dataItems.add(new PagerProvider.DataItem("entry/resources/base/media/TwinklingRefreshLayout.mp4"));
        dataItems.add(new PagerProvider.DataItem("entry/resources/base/media/TwinklingRefreshLayout.mp4"));

        return dataItems;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        LogUtil.info(TAG, "onInactive is called");
        super.onInactive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        LogUtil.info(TAG, "onStop is called");
        super.onStop();
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
    }
}
