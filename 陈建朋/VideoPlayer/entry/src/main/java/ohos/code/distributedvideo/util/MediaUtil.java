/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.code.distributedvideo.util;

import ohos.code.distributedvideo.ResourceTable;
import ohos.code.distributedvideo.data.VideoListMo;

import java.util.ArrayList;
import java.util.List;

/**
 * Media util
 *
 * @since 2020-12-04
 */
public class MediaUtil {
    private static List<VideoListMo> playListMos = new ArrayList<>(0);

    private MediaUtil() {
    }

    static {
        playListMos.add(new VideoListMo(ResourceTable.Media_photo1,
                "“码”力全开：挑战DIY HarmonyOS视频播放器，赢取重磅好礼！", "开发者可以根据自己的需求，结合HarmonyOS媒体组件和分布式能力等，构建一款属于你的视频展示应用。应用需实现滑动切换视频功能、评论功能、分布式流转功能等。\n" +
                "\n" +
                "视频中融入“HDC2021”、“我期待”和“HarmonyOS”。"));
        playListMos.add(new VideoListMo(ResourceTable.Media_photo2, "“码”力全开：挑战DIY HarmonyOS视频播放器，赢取重磅好礼！", "开发者可以根据自己的需求，结合HarmonyOS媒体组件和分布式能力等，构建一款属于你的视频展示应用。应用需实现滑动切换视频功能、评论功能、分布式流转功能等。\n" +
                "\n" +
                "视频中融入“HDC2021”、“我期待”和“HarmonyOS”。"));
        playListMos.add(new VideoListMo(ResourceTable.Media_photo3, "“码”力全开：挑战DIY HarmonyOS视频播放器，赢取重磅好礼！", "开发者可以根据自己的需求，结合HarmonyOS媒体组件和分布式能力等，构建一款属于你的视频展示应用。应用需实现滑动切换视频功能、评论功能、分布式流转功能等。\n" +
                "\n" +
                "视频中融入“HDC2021”、“我期待”和“HarmonyOS”。"));
    }

    /**
     * get VideoListMos
     *
     * @return VideoListMo
     */
    public static List<VideoListMo> getPlayListMos() {
        return playListMos;
    }
}
