package com.cxs.videoplayer.data;

public class CommentInfo {
    String name;
    String content;
    String subDate;

    public CommentInfo(String name, String content, String subDate) {
        this.name = name;
        this.content = content;
        this.subDate = subDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubDate() {
        return subDate;
    }

    public void setSubDate(String subDate) {
        this.subDate = subDate;
    }
}
