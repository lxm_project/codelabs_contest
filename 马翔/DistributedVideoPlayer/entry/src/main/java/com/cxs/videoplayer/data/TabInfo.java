package com.cxs.videoplayer.data;

public class TabInfo {

    String title;
    int xmlId;

    public TabInfo(String title, int xmlId) {
        this.title = title;
        this.xmlId = xmlId;
    }

    public String getTitle() {
        return title;
    }

    public int getXmlId() {
        return xmlId;
    }
}
