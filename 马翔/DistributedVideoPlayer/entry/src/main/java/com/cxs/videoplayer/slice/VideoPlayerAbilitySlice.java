package com.cxs.videoplayer.slice;

import com.cxs.videoplayer.ResourceTable;
import com.cxs.videoplayer.component.VideoListView;
import com.cxs.videoplayer.constant.ControllCode;
import com.cxs.videoplayer.data.PlayInfo;
import com.cxs.videoplayer.data.VideoInfo;
import com.cxs.videoplayer.player.HmPlayer;
import com.cxs.videoplayer.component.CommentListView;
import com.cxs.videoplayer.component.DeviceListView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.accessibility.ability.SoftKeyBoardController;
import ohos.accessibility.ability.SoftKeyBoardListener;
import ohos.agp.components.*;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.window.service.WindowManager;

import java.util.List;


public class VideoPlayerAbilitySlice extends AbilitySlice implements IAbilityContinuation {

    public HmPlayer hmPlayer = null;
    private PlayInfo playInfo = null;
    private final static int DEFAULT_PAGE = 0;
    private TabList tabList;
    private DirectionalLayout videoLayout;
    private ScrollView scrollView;
    String[] tabs = new String[]{
            "列表", "评论", "设备"
    };



    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_video_player);
        onActive();
        initView();
        if (playInfo != null) {
            hmPlayer.execute(playInfo);
        }

    }

    private void initView() {
        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        videoLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_video_player);
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        scrollView = (ScrollView) findComponentById(ResourceTable.Id_scroll_view);

        createVideoPlayer();
        initTablist();
    }

    private void createVideoPlayer() {
        // 创建本地播放器
        hmPlayer = new HmPlayer(getContext());
        // 创建播放界面
        DirectionalLayout controllerView = (DirectionalLayout) LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_video_controller, null, false);
        SurfaceProvider surfaceProvider = hmPlayer.getSurfaceProvider();

        videoLayout.addComponent(surfaceProvider);
        videoLayout.addComponent(controllerView);

        hmPlayer.setVideoView(controllerView);
    }

    private void initTablist() {
        for (String tab : tabs) {
            tabList.addTab(createTab(tab));
        }
        tabList.setFixedMode(true);

        VideoListView videoListView = new VideoListView(getContext(), hmPlayer);
        scrollView.addComponent(videoListView.getView());
        tabList.selectTabAt(DEFAULT_PAGE);

        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                switch (tab.getPosition()) {
                    case 0: {
                        VideoListView videoListView = new VideoListView(getContext(), hmPlayer);

                        scrollView.addComponent(videoListView.getView());
                        break;
                    }
                    case 1: {
                        CommentListView commentListView = new CommentListView(getContext());
                        scrollView.addComponent(commentListView.getView());
                        break;
                    }
                    case 2: {
                        DeviceListView deviceListView = new DeviceListView(getContext(), VideoPlayerAbilitySlice.this);
                        scrollView.addComponent(deviceListView.getView());
                    }
                }
            }

            @Override
            public void onUnselected(TabList.Tab tab) {
                scrollView.removeAllComponents();
            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
    }

    private TabList.Tab createTab(String tabName) {
        // 创建选项卡实例
        TabList.Tab tab = tabList.new Tab(this);
        tab.setText(tabName);
        tab.setPadding(20, 0, 20, 0);
        return tab;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        hmPlayer.getSurfaceProvider();
    }


    @Override
    public boolean onStartContinuation() {
        return true;
    }

    /**
     * 源侧
     * @param intentParams
     * @return
     */
    @Override
    public boolean onSaveData(IntentParams intentParams) {
        intentParams.setParam("index", hmPlayer.getVideoInfo().getIndex());
//        System.out.println("onSaveData1");
        intentParams.setParam("status", hmPlayer.getStatus().getCode());
        intentParams.setParam("current", hmPlayer.getCurrentTime());
//        System.out.println("onSaveData");
        return true;
    }

    /**
     * 目标侧
     * @param intentParams
     * @return
     */
    @Override
    public boolean onRestoreData(IntentParams intentParams) {
//        System.out.println("onRestoreData");
        int index = (int) intentParams.getParam("index");
        System.out.println(index);
        List<VideoInfo> videoInfos = VideoListView.getVideoInfos();
        VideoInfo videoInfo = videoInfos.get(index);

        int value = (int)intentParams.getParam("status");
        ControllCode status = ControllCode.getByValue(value);
        long currentTime = (long) intentParams.getParam("current");
//
        playInfo = new PlayInfo(videoInfo, status, currentTime);
        return true;
    }

    @Override
    public void onCompleteContinuation(int i) {

    }
}
